## 0.2.3（2021-05-10）
- feat: 增加 `canvasToTempFilePath` 方法，用于生成图片
```js
this.$refs.chart.canvasToTempFilePath({success: (res) => {
	console.log('tempFilePath:', res.tempFilePath)
}})
```
## 0.2.2（2021-05-10）
- feat: 增加 `dispose` 方法，用于销毁实例
- feat: 增加 `isClickable` 是否派发点击
- feat: 实验性的支持 `nvue` 使用要慎重考虑
- ## [代码示例：http://liangei.gitee.io/limeui/#/echart-example](http://liangei.gitee.io/limeui/#/echart-example)
## 0.2.1（2021-05-06）
- fix：修复 微信小程序 json 报错
- chore: `reset` 更改为 `setChart`
- feat: 增加 `isEnable` 开启初始化 启用这个后 无须再使用`init`方法
```html
<l-echart ref="chart" is-enable />
```
```js
// 显示加载
this.$refs.chart.showLoading()
// 使用实例回调
this.$refs.chart.setChart(chart => ...code)
// 直接设置图表配置
this.$refs.chart.setOption(data)
```
## 0.2.0（2021-05-05）
- fix：修复 头条 百度 偏移的问题
- docs: 更新文档
## [代码示例：http://liangei.gitee.io/limeui/#/echart-example](http://liangei.gitee.io/limeui/#/echart-example)
## 0.1.0（2021-05-02）
- chore:  第一次上传，基本全端兼容，使用方法与官网一致。
- 已知BUG：非2d 无法使用背景色，已反馈官方
- 已知BUG：头条 百度 有许些偏移
- 后期计划：兼容nvue
