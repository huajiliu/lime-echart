export default class Canvas {
	constructor(ctx, canvasId, isNew, canvasNode) {
		this.ctx = ctx;
		this.canvasId = canvasId;
		this.chart = null;
		this.isNew = isNew
		if (isNew) {
			this.canvasNode = canvasNode;
		} else {
			this._initStyle(ctx);
		}
		this._initEvent();
	}

	getContext(contextType) {
		if (contextType === '2d') {
			return this.ctx;
		}
	}
	setChart(chart) {
		// this.chart = chart;
	}

	attachEvent(e) {
		// noop
	}

	detachEvent() {
		// noop
	}
	_initCanvas(zrender, ctx) {
		zrender.util.getContext = function() {
			return ctx;
		};
		zrender.util.$override('measureText', function(text, font) {
			ctx.font = font || '12px sans-serif';
			return ctx.measureText(text, font);
		});
	}
	strLen(str) {
		let len = 0;
		for (let i = 0; i < str.length; i++) {
			if (str.charCodeAt(i) > 0 && str.charCodeAt(i) < 128) {
				len++;
			} else {
				len += 2;
			}
		}
		return len;
	}
	_initStyle(ctx) {
		var styles = [
			'fillStyle',
			'strokeStyle',
			'fontSize',
			'globalAlpha',
			'opacity',
			'textAlign',
			'textBaseline',
			'shadow',
			'lineWidth',
			'lineCap',
			'lineJoin',
			'lineDash',
			'miterLimit',
			// #ifdef MP-TOUTIAO
			'font'
			// #endif
		];
		const fontSizeReg = /([\d\.]+)px/;
		const colorReg = /#([0-9a-fA-F])([0-9a-fA-F])([0-9a-fA-F])(['"])/g;
		styles.forEach(style => {
			Object.defineProperty(ctx, style, {
				set: value => {
					if (style === 'font' && fontSizeReg.test(val)) {
						const match = fontSizeReg.exec(val);
						ctx.setFontSize(match[1]);
						return;
					}
					if (style === 'opacity') {
						ctx.setGlobalAlpha(value)
						return;
					}
					if (style !== 'fillStyle' && style !== 'strokeStyle' || value !== 'none' && value !== null) {
						// #ifdef H5 || APP-PLUS
						if(typeof value == 'object') {
							if (value?.colorStop) {
								ctx['set' + style.charAt(0).toUpperCase() + style.slice(1)](value);
							}
							return
						} 
						// #endif
						if(colorReg.test(value)) {
							value = value.replace(colorReg, '#$1$1$2$2$3$3$4')
						}
						ctx['set' + style.charAt(0).toUpperCase() + style.slice(1)](value);
					}
				}
			});
		});

		ctx.createRadialGradient = () => {
			return ctx.createCircularGradient(arguments);
		};
		// 字节不支持
		if (!ctx.strokeText) {
			ctx.strokeText = (...a) => {
				ctx.fillText(...a)
			}
		}
		// 钉钉不支持 
		if (!ctx.measureText) {
			ctx.measureText = (text, font) => {
				let fontSize = 12;
				if (font) {
					fontSize = parseInt(font.match(/([\d\.]+)px/)[1])
				}
				fontSize /= 2;
				return {
					width: this.strLen(text) * fontSize
				};
			}
		}
	}

	_initEvent() {
		this.event = {};
		const eventNames = [{
			wxName: 'touchStart',
			ecName: 'mousedown'
		}, {
			wxName: 'touchMove',
			ecName: 'mousemove'
		}, {
			wxName: 'touchEnd',
			ecName: 'mouseup'
		}, {
			wxName: 'touchEnd',
			ecName: 'click'
		}];

		eventNames.forEach(name => {
			this.event[name.wxName] = e => {
				const touch = e.touches[0];
				this.chart.getZr().handler.dispatch(name.ecName, {
					zrX: name.wxName === 'tap' ? touch.clientX : touch.x,
					zrY: name.wxName === 'tap' ? touch.clientY : touch.y
				});
			};
		});
	}

	set width(w) {
		if (this.canvasNode) this.canvasNode.width = w
	}
	set height(h) {
		if (this.canvasNode) this.canvasNode.height = h
	}

	get width() {
		if (this.canvasNode)
			return this.canvasNode.width
		return 0
	}
	get height() {
		if (this.canvasNode)
			return this.canvasNode.height
		return 0
	}
}
